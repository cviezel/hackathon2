import datetime
import pandas as pd
import pandas_datareader as pdr
import matplotlib.pyplot as plt
from flask import Flask, render_template
from flask_restplus import Resource, Api, fields
from flask.helpers import make_response

app = Flask(__name__)
# Number of seconds to wait before files and images expire
app.config['SEND_FILE_MAX_AGE_DEFAULT'] = 1
api = Api(app)

@api.route('/advice/<string:ticker>')
class BollingerHTML(Resource):
    def predict(self, df):
            curr_price = df.tail()['Adj Close'][0]
            upper_band = df.tail()['Upper Band'][0]
            lower_band = df.tail()['Lower Band'][0]
            if curr_price < lower_band:
                return "BUY"
            elif curr_price > upper_band:
                return "SELL"  
            return "HOLD"

    def get(self, ticker):
        try:      
            df = pdr.get_data_yahoo(ticker)
        except:
            return 'Invalid ticker entered'
        
        df['30d avg'] = df['Adj Close'].rolling(window=30).mean()
        df['30d std'] = df['Adj Close'].rolling(window=30).std()
        df['Upper Band'] = df['30d avg'] + (df['30d std'] * 2)
        df['Lower Band'] = df['30d avg'] - (df['30d std'] * 2)

        # set style, empty figure and axes
        plt.style.use('bmh')
        fig = plt.figure(figsize=(12,6))
        ax = fig.add_subplot(111)

        # Get index values for the X axis for the DataFrame
        x_axis = df['2020':].index.get_level_values(0)

        # Plot shaded 21 Day Bollinger Band for Facebook
        ax.fill_between(x_axis, df['2020':]['Upper Band'], df['2020':]['Lower Band'], color='grey')

        ax.plot(x_axis, df['2020':]['Adj Close'], color='blue', lw=2)
        ax.plot(x_axis, df['2020':]['30d avg'], color='black', lw=2)

        # Set Title & Show the Image
        ax.set_title('30 Day Bollinger Band For ' + ticker)
        ax.set_xlabel('Date (Year/Month)')
        ax.set_ylabel('Price(USD)')

        plt.savefig('hackathon2/static/band.png')
        
        ## Add response to the html
        advice = self.predict(df)
        
        headers = {'Content-Type': 'text/html'}
        return make_response(render_template("index.html", var = advice), 200, headers)


if __name__ == '__main__':
    app.run(debug=True)
